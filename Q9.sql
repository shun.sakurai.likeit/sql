﻿SELECT
t1.category_name,
SUM(t.item_price) AS total_price
FROM
item_category t1
INNER JOIN
item t
ON
t1.category_id = t.category_id
GROUP BY
t1.category_name
ORDER BY
total_price DESC
